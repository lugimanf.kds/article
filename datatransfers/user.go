package datatransfers

type User struct {
	ID      int64 `json:"id"`
	Name    string `json:"name"`
	Address string `json:"address"`
	Msisdn  string `json:"msisdn"`
}

type ResponseUser struct {
	StatusCode int    		`json:"status_code"`
	Message    string 		`json:"message"`
	Data	   string	  	`json:"data"`

}