package datatransfers

type ResponseArticle struct {
	ID		int64     `json:"id"`
	Title	string    `json:"title"`
	Content	string    `json:"content"`
	Author	string    `json:"author"`
}