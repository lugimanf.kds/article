package models

import (
	"fmt"

	"gitlab.com/lugimanf.kds/gocommon/mysql"
)

type Article struct {
	ID      	int64 `json:"id" bson:"id"`
	UserID  	int64 `json:"user_id" bson:"user_id" validate:"required"`
	Title 		string `json:"title" bson:"title" validate:"required"`
	Content    	string `json:"content" bson:"content" validate:"required"`
}

type ArticleInt interface {
	GetByID(userID int64) (*Article, error)
	InsertArticle(user Article) (*Article, error)
	UpdateArticle(user Article) (*Article, error)
	DeleteArticle(ID int64) (error)
}

type ArticleOrm struct {
	Client    mysql.MysqlConInt
	TableName string
}

func NewArticleOrm() ArticleInt {
	con, _:= mysql.MySQLCon.ConnectDB()
	return &ArticleOrm{
		con,
		"article",
	}
}

func (a *ArticleOrm) GetByID(articleID int64) (*Article, error) {
	defer a.Client.Close()
	var result = Article{}
	query := fmt.Sprintf("select id, user_id, title, content from %v where id = '%v'", a.TableName, articleID)
	err := a.Client.QueryRow(query).Scan(&result.ID, &result.UserID, &result.Title, &result.Content)
	if err != nil {
		return nil, err
	}
	return &result, nil
}

func (a *ArticleOrm) InsertArticle(article Article) (*Article, error) {
	stmt, err:= a.Client.Prepare("insert article set user_id=?, title=?, content=?")
	if err != nil {
		return nil, err
	}
	defer stmt.Close()
	res, err:= stmt.Exec(article.UserID, article.Title, article.Content)
	ID, err := res.LastInsertId()
	if err != nil{
		return nil, err
	}
	return a.GetByID(ID)
}

func (a *ArticleOrm) UpdateArticle(article Article) (*Article, error) {
	_, err:= a.Client.Exec("update article set content=?, title=? where id=?", article.Content, article.Title, article.ID)
	if err != nil{
		return nil, err
	}
	return &article, nil
}

func (a *ArticleOrm) DeleteArticle(ID int64) (error) {
	defer a.Client.Close()
	_, err:= a.Client.Exec("delete from article where id = ?", ID)
	if err != nil{
		return err
	}
	return nil
}