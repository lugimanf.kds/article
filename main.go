package main

import (
	"fmt"
	"net/http"
	"os"

	"github.com/go-chi/chi"
	"github.com/sirupsen/logrus"

	"gitlab.com/lugimanf.kds/gocommon/config"
	"gitlab.com/lugimanf.kds/gocommon/middleware"
	"gitlab.com/lugimanf.kds/gocommon/mysql"
	"gitlab.com/lugimanf.kds/gocommon/redis"
	"gitlab.com/lugimanf.kds/article/controllers"
)

func init() {
	config.SetupConfigEnv()
	redis.CreateConnection(redis.RedisConnection{os.Getenv("REDIS_CONNECTION")})
	mysql.CreateConnection(os.Getenv("MYSQL_CONNECTION"))
	port = os.Getenv("PORT")
}

var port string

func main() {
	r := chi.NewRouter()
	//article
	r.Group(func(r chi.Router) {
		r.Use(middleware.ThrottleRequest)
		r.Get("/article/{id}/", controllers.ArticleByID)
		r.Post("/article/", controllers.InsertArticle)
		r.Put("/article/{id}/", controllers.UpdateArticle)
		r.Delete("/article/{id}/", controllers.DeleteArticle)

	})
	logrus.Infof("[SERVER] service %v server in port :%v", os.Getenv("SERVICE_NAME"), port)
	logrus.Fatalln(http.ListenAndServe(fmt.Sprintf(":%v", port), r))

}
