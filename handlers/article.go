package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/lugimanf.kds/gocommon/redis"
	"gitlab.com/lugimanf.kds/article/models"
	"gitlab.com/lugimanf.kds/article/curls"
	"gitlab.com/lugimanf.kds/article/datatransfers"
)

type ArticleHandler struct {
	ArticleOrm models.ArticleInt
	RedisCon redis.RedisInt
	UserCurl curls.CurlUserInt
}

type ArticleHandlerInt interface {
	ArticleByID(ID int64) (*datatransfers.ResponseArticle, error)
	InsertArticle(article models.Article) (*datatransfers.ResponseArticle, error)
	UpdateArticleByID(article models.Article) (*datatransfers.ResponseArticle, error)
	DeleteArticleByID(ID int64) (error)
}

func NewArticleHandler(articleOrm models.ArticleInt, redisCon redis.RedisInt, userCurl curls.CurlUserInt) ArticleHandlerInt {
	return &ArticleHandler{
		ArticleOrm: articleOrm,
		RedisCon: redisCon,
		UserCurl: userCurl,
	}
}

func (u *ArticleHandler) ArticleByID(ID int64) (*datatransfers.ResponseArticle, error) {
	rArticle:= datatransfers.ResponseArticle{}
	keyRedis := fmt.Sprintf("article_%v", ID)
	article := &models.Article{}
	redisResult, err := u.RedisCon.Get(keyRedis)
	if err != nil {
		article, err = u.ArticleOrm.GetByID(ID)
		if err != nil {
			return nil, err
		}

		user, statusCode, err := u.UserCurl.UserByID(article.UserID)
		if err != nil || *statusCode != http.StatusOK{
			return nil, err
		}

		rArticle = datatransfers.ResponseArticle{
			ID: article.ID,
			Title: article.Title,
			Content: article.Content,
			Author: user.Name,
		}
		//set data user to cache
		resultMarshal, err := json.Marshal(rArticle)
		if err != nil {
			return nil, err
		}
		u.RedisCon.Set(keyRedis, string(resultMarshal), 3600*time.Second)
	} else {
		err = json.Unmarshal([]byte(*redisResult), &rArticle)
		if err != nil {
			return nil, err
		}
	}
	return &rArticle, nil
}

func (u *ArticleHandler) InsertArticle(article models.Article) (*datatransfers.ResponseArticle, error) {
	articleByID, err:= u.ArticleOrm.InsertArticle(article)
	if err != nil{
		return nil, err
	}
	return u.ArticleByID(articleByID.ID)
}

func (u *ArticleHandler) UpdateArticleByID(article models.Article) (*datatransfers.ResponseArticle, error) {
	articleByID, err:= u.ArticleOrm.UpdateArticle(article)
	if err != nil{
		return nil, err
	}
	keyRedis := fmt.Sprintf("article_%v", articleByID.ID)
	err = u.RedisCon.Del(keyRedis)
	if err != nil{
		return nil, err
	}
	return u.ArticleByID(articleByID.ID)
}

func (u *ArticleHandler) DeleteArticleByID(ID int64) (error) {
	err := u.ArticleOrm.DeleteArticle(ID)
	if err != nil{
		return err
	}
	return nil
}