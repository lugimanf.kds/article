package controllers

import (
	"database/sql"
	"fmt"
	"github.com/go-chi/chi"
	"gitlab.com/lugimanf.kds/gocommon/redis"
	"gitlab.com/lugimanf.kds/gocommon/response"
	"gitlab.com/lugimanf.kds/gocommon/utils"
	"gitlab.com/lugimanf.kds/article/curls"
	"gitlab.com/lugimanf.kds/article/handlers"
	"gitlab.com/lugimanf.kds/article/models"
	"net/http"
	"strconv"
)

func ArticleByID(w http.ResponseWriter, r *http.Request) {
	paramID := chi.URLParam(r, "id")
	articleOrm := models.NewArticleOrm()
	userCurl := curls.NewCurlUser()
	userHandler := handlers.NewArticleHandler(articleOrm, redis.ConRedis, userCurl)
	ID, err := strconv.ParseInt(paramID, 10, 64)
	if err != nil {
		_ = response.ResponseAes256(w, response.ParameterResponse{
			Status:  http.StatusInternalServerError,
			Message: "Internal Error Please Call Administator",
		})
		return
	}

	Article, err := userHandler.ArticleByID(ID)
	if err != nil {
		if sql.ErrNoRows.Error() == err.Error() {
			_ =	response.Response(w, response.ParameterResponse{
				Status:  http.StatusNotFound,
				Message: "User not found",
			})
			return
		}

		_ = response.Response(w, response.ParameterResponse{
			Status:  http.StatusInternalServerError,
			Message: "Internal Error Please Call Administator",
		})
		return
	}

	_ = response.ResponseAes256(w, response.ParameterResponse{
		Status:  http.StatusOK,
		Message: "",
		Data:    Article,
	})
}

func InsertArticle(w http.ResponseWriter, r *http.Request) {
	paramUserID:= r.FormValue("user_id")
	userID, err := strconv.ParseInt(paramUserID, 10, 64)
	if err != nil {
		_ = response.ResponseAes256(w, response.ParameterResponse{
			Status:  http.StatusInternalServerError,
			Message: "Internal Error Please Call Administator",
		})
		return
	}
	article := &models.Article{
		UserID: userID,
		Title: r.FormValue("title"),
		Content: r.FormValue("content"),
	}
	articleOrm := models.NewArticleOrm()
	userCurl := curls.NewCurlUser()
	articleHandler := handlers.NewArticleHandler(articleOrm, redis.ConRedis, userCurl)

	//validation payload
	err = utils.Validator(article)
	if err != nil {
		_ = response.Response(w, response.ParameterResponse{
			Status:  http.StatusBadRequest,
			Message: err.Error(),
		})
		return
	}

	rArticle, err := articleHandler.InsertArticle(*article)
	if err != nil {
		_ = response.Response(w, response.ParameterResponse{
			Status:  http.StatusInternalServerError,
			Message: "Internal Error Please Call Administator",
		})
		return
	}

	_ = response.ResponseAes256(w, response.ParameterResponse{
		Status:  http.StatusOK,
		Message: "",
		Data:    rArticle,
	})
}

func UpdateArticle(w http.ResponseWriter, r *http.Request) {
	paramID := chi.URLParam(r, "id")
	paramUserID := r.FormValue("user_id")
	articleOrm := models.NewArticleOrm()
	userCurl := curls.NewCurlUser()
	articleHandler := handlers.NewArticleHandler(articleOrm, redis.ConRedis, userCurl)

	ID, err := strconv.ParseInt(paramID, 10, 64)
	if err != nil {
		_ = response.Response(w, response.ParameterResponse{
			Status:  http.StatusInternalServerError,
			Message: "Internal Error Please Call Administator",
		})
		return
	}

	UserID, err := strconv.ParseInt(paramUserID, 10, 64)
	if err != nil {
		_ = response.Response(w, response.ParameterResponse{
			Status:  http.StatusInternalServerError,
			Message: "Internal Error Please Call Administator",
		})
		return
	}

	article := models.Article{
		ID: ID,
		Title: r.FormValue("title"),
		Content: r.FormValue("content"),
		UserID: UserID,
	}

	//validation payload
	err = utils.Validator(article)
	if err != nil {
		_ = response.Response(w, response.ParameterResponse{
			Status:  http.StatusBadRequest,
			Message: err.Error(),
		})
		return
	}

	rArticle, err := articleHandler.UpdateArticleByID(article)
	if err != nil {
		fmt.Println(err.Error())
		if err.Error() == sql.ErrNoRows.Error() {
			_ = response.Response(w, response.ParameterResponse{
				Status:  http.StatusBadRequest,
				Message: "Article not found",
			})
			return
		}

		_ = response.Response(w, response.ParameterResponse{
			Status:  http.StatusInternalServerError,
			Message: "Internal Error Please Call Administator",
		})
		return
	}

	_ = response.ResponseAes256(w, response.ParameterResponse{
		Status:  http.StatusOK,
		Message: "Update Success",
		Data:    rArticle,
	})
}

func DeleteArticle(w http.ResponseWriter, r *http.Request) {
	paramID := chi.URLParam(r, "id")
	articleOrm := models.NewArticleOrm()
	userCurl := curls.NewCurlUser()
	articleHandler := handlers.NewArticleHandler(articleOrm, redis.ConRedis, userCurl)
	ID, err := strconv.ParseInt(paramID, 10, 64)
	if err != nil {
		_ = response.Response(w, response.ParameterResponse{
			Status:  http.StatusInternalServerError,
			Message: "Internal Error Please Call Administator",
		})
		return
	}

	err = articleHandler.DeleteArticleByID(ID)
	if err != nil {
		if err.Error() == sql.ErrNoRows.Error() {
			_ = response.Response(w, response.ParameterResponse{
				Status:  http.StatusBadRequest,
				Message: "Article not found",
			})
			return
		}
		_ = response.Response(w, response.ParameterResponse{
			Status:  http.StatusInternalServerError,
			Message: "Internal Error Please Call Administator",
		})
		return
	}

	_ = response.Response(w, response.ParameterResponse{
		Status:  http.StatusOK,
		Message: "Delete Success",
		Data:    nil,
	})
}