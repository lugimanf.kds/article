package curls

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/lugimanf.kds/gocommon/curl"
	"gitlab.com/lugimanf.kds/gocommon/utils"
	"gitlab.com/lugimanf.kds/article/datatransfers"
	"net/http"
	"os"
	"strings"
)

type CurlUserInt interface{
	UserByID(ID int64) (*datatransfers.User, *int, error)
}

type CurlUser struct {
}

func NewCurlUser() CurlUserInt{
	return &CurlUser{}
}

func(u *CurlUser) UserByID(ID int64) (*datatransfers.User, *int, error){
	response := datatransfers.ResponseUser{}
	user:= datatransfers.User{}
	url:= fmt.Sprintf("%v%v", os.Getenv("SERVICE_USER_HOSTNAME"), os.Getenv("SERVICE_USER_GET_USER"))
	url = strings.Replace(url, "{id}", fmt.Sprintf("%v", ID), -1)
	curl := curl.NewCurl(url, http.MethodGet)
	res, statusCode, err := curl.Curl()
	if  err != nil {
		return nil, statusCode, err
	}
	err = json.Unmarshal(res, &response)
	if  err != nil {
		return nil, nil, err
	}

	if *statusCode != http.StatusOK {
		return nil, statusCode, errors.New(response.Message)
	}

	decode := utils.DecryptAes256(response.Data, os.Getenv("KEY_AES256"))
	err = json.Unmarshal([]byte(decode), &user)
	if  err != nil {
		return nil, nil, err
	}

	return &user, statusCode, nil
}
